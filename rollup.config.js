import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import globals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';

export default {
  input: 'src/index.js',
  output: {
    file: 'build/index.js',
    format: 'cjs',
    external: [
      'lodash'
    ],
    plugins: [
      nodeResolve({ preferBuiltins: true }), // or `true`
      commonjs({
        include: 'node_modules/**'
      }),
      globals(),
      builtins()
    ]
  }
};
