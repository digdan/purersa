import BigInteger from 'big-integer';
import basex from 'base-x';
import { Buffer } from 'buffer';

const BASE58 = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
const bs58 = basex(BASE58);
const EXPON = 65537;
const PRIMECHECK = 50;

//Psudo Random Number Generator
const prng = {
  nextBytes: (len, dec=false) => {
    if (dec) {
      return Array(len)
            .fill()
            .map(() => parseInt((Math.round(Math.random() * 256)).toString(10)))
            .join('')
    } else {
      return Array(len)
            .fill()
            .map(() => (Math.round(Math.random() * 256)).toString(16))
            .join('')
    }
  }
};

// Key Class, should divide out between public/private
class PureRSAKey {
  constructor() {
    this.n = null; // Private & Public
    this.e = 0; // Private & Public
    this.d = null; // Private
    this.p = null;
    this.q = null;
    this.dmp1 = null;
    this.dmq1 = null;
    this.coeff = null;
  }
  publicExport() {
    return bs58.encode(new Buffer(this.n.toArray(256).value));
  }
  publicImport(bn) {
    this.n = bs58.decode(bn);
    this.e = parseInt(EXPON, 16);
  }
}

// Generates a public key from a private key
const publicFromPrivate = privateKey => {
  const publicKey = new PureRSAKey();
  publicKey.n = privateKey.n;
  publicKey.e = privateKey.e;
  return publicKey;
}

// Turns integer into text
const pkcs1unpad2 = (d, n) => {
  var b = d.toArray(256).value;
  var i = 0;
  while(i < b.length && b[i] == 0) ++i;
  if(b.length-i != n-1 || b[i] != 2) {
    console.log('bad decrypt input');
    return null;
  }
  ++i;
  while(b[i] != 0)
    if(++i >= b.length) return null;
  var ret = "";
  while(++i < b.length) {
    var c = b[i] & 255;
    if(c < 128) { // utf-8 decode
      ret += String.fromCharCode(c);
    }
    else if((c > 191) && (c < 224)) {
      ret += String.fromCharCode(((c & 31) << 6) | (b[i+1] & 63));
      ++i;
    }
    else {
      ret += String.fromCharCode(((c & 15) << 12) | ((b[i+1] & 63) << 6) | (b[i+2] & 63));
      i += 2;
    }
  }
  return ret;
}

// Turns text into an integer
const pkcs1pad2 = (s, n) => {
  if(n < s.length + 11) { // TODO: fix for utf-8
    console.log("Message too long for RSA");
    return null;
  }
  var ba = new Array();
  var i = s.length - 1;
  while(i >= 0 && n > 0) {
    var c = s.charCodeAt(i--);
    if(c < 128) { // encode using utf-8
      ba[--n] = c;
    }
    else if((c > 127) && (c < 2048)) {
      ba[--n] = (c & 63) | 128;
      ba[--n] = (c >> 6) | 192;
    }
    else {
      ba[--n] = (c & 63) | 128;
      ba[--n] = ((c >> 6) & 63) | 128;
      ba[--n] = (c >> 12) | 224;
    }
  }
  ba[--n] = 0;
  var x = new Array();
  while(n > 2) { // random non-zero pad
    x[0] = 0;
    while(x[0] == 0) x[0] = parseInt(prng.nextBytes(x, true));
    ba[--n] = x[0];
  }
  ba[--n] = 2;
  ba[--n] = 0;
  return new BigInteger.fromArray(ba, 256);
}

// Key generation
const generateRSAKeys = (keySize) => {
  const privateKey = new PureRSAKey();
  const rng = prng;
  const qs = keySize >> 1; //Public Key Size?
  privateKey.e = parseInt(EXPON, 16);
  const ee = new BigInteger(EXPON, 16);
  for(;;) {
    for(;;) {
      //privateKey.p = new BigInteger(parseInt(keySize - qs), 1, rng);
      privateKey.p = new BigInteger(rng.nextBytes(keySize - qs), 16);
      while(!privateKey.p.isProbablePrime(PRIMECHECK)) {
        privateKey.p = new BigInteger(rng.nextBytes(keySize - qs), 16);
      }
      console.log('primed a');
      privateKey.p.subtract(BigInteger.one);
      if (BigInteger.gcd(privateKey.p, ee).compareTo(BigInteger.one) === 0 && privateKey.p.isProbablePrime(PRIMECHECK)) break;
    }
    for(;;) {
      privateKey.q = new BigInteger(rng.nextBytes(qs), 16);
      while(!privateKey.q.isProbablePrime(PRIMECHECK)) {
        privateKey.q = new BigInteger(rng.nextBytes(qs), 16);
      }
      console.log('primed b')
      privateKey.q.subtract(BigInteger.one);
      if (BigInteger.gcd(privateKey.q, ee).compareTo(BigInteger.one) === 0 && privateKey.q.isProbablePrime(PRIMECHECK)) break;
    }
    if (privateKey.p.compareTo(privateKey.q) <= 0) {
      const t = privateKey.p;
      privateKey.p = privateKey.q;
      privateKey.q = t;
    }
    const p1 = privateKey.p.subtract(BigInteger.one);
    const q1 = privateKey.q.subtract(BigInteger.one);
    const phi = p1.multiply(q1);
    if (BigInteger.gcd(phi, ee).compareTo(BigInteger.one) === 0) {
      privateKey.n = privateKey.p.multiply(privateKey.q);
      privateKey.d = ee.modInv(phi);
      privateKey.dmp1 = privateKey.d.mod(p1);
      privateKey.dmq1 = privateKey.d.mod(q1);
      privateKey.coeff = privateKey.q.modInv(privateKey.p);
      break;
    }
  }
  const publicKey = publicFromPrivate(privateKey);
  return {
    publicKey,
    privateKey
  };
}

// Public key encryption
function doPublic(x, key) {
  return x.modPow(key.e, key.n);
}

// Private key decryption
function doPrivate(x, key) { //Suspect problem here
  if(key.p == null || key.q == null)
    return x.modPow(this.d, this.n);

  // TODO: re-calculate any missing CRT params
  var xp = x.mod(key.p).modPow(key.dmp1, key.p);
  var xq = x.mod(key.q).modPow(key.dmq1, key.q);

  while(xp.compareTo(xq) < 0)
    xp = xp.add(key.p);
  return xp.subtract(xq).multiply(key.coeff).mod(key.p).multiply(key.q).add(xq);
}

// Encrypt from public/private key
function encrypt(text, key) {
  var m = pkcs1pad2(text, (key.n.bitLength()+7) >> 3);
  if(m == null) return null;
  var c = doPublic(m, key);
  if(c == null) return null;
  return bs58.encode(new Buffer(c.toArray(256).value));
}

// Decrypt from private key
function decrypt(enc, key) {
  var c = new BigInteger.fromArray([...new Buffer(bs58.decode(enc))], 256);
  var m = doPrivate(c, key);
  if(m == null) return null;
  return pkcs1unpad2(m, (key.n.bitLength()+7)>>3);
}

export {
  generateRSAKeys,
  encrypt,
  decrypt
}
