import {
  generateRSAKeys,
  encrypt,
  decrypt
} from './lib/PureRSA';
const keyStarted = Date.now();
const {privateKey, publicKey} = generateRSAKeys(256);
const keyEnded = Date.now();

function doEncDec(keys) {
  const {privateKey, publicKey} = keys;
  const payload = (Math.floor(Math.random() * 10000000000000) + 10).toString();
  const enc = encrypt(payload, publicKey);
  const dec = decrypt(enc, privateKey);
  console.log('in:', payload);
  console.log('enc: ', enc);
  console.log('dec: ', dec);
  console.log('----------------');
}

let started = Date.now();
for(let i=0;i<100;i++) {
  doEncDec({
    privateKey,
    publicKey
  })
}
console.log('keygen took', (keyEnded - keyStarted));
console.log('tests took', (Date.now() - started));
